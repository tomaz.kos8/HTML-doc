Name: Input_typedef.ctl
DisplayName: Input_typedef.ctl
Path: D:\LabVIEW_custom_library\absolute_time_graph\Input_typedef.ctl
TypeString: VI
VIType: Control VI
CodeSize: N/A
Callers: absolute_time_graph.lvlib:absolute_time_graph_main.vi, 
Description: Typedef for graph input data.