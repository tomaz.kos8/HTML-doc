Name: graph states typedef.ctl
DisplayName: graph states typedef.ctl
Path: D:\LabVIEW_custom_library\absolute_time_graph\graph states typedef.ctl
TypeString: VI
VIType: Control VI
CodeSize: N/A
Callers: absolute_time_graph.lvlib:absolute_time_graph_main.vi, 
Description: Typedef for graph state machine states.