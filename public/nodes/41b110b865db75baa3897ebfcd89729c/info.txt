Name: custom_range_stop_button.ctl
DisplayName: custom_range_stop_button.ctl
Path: D:\LabVIEW_custom_library\absolute_time_graph\custom_range_stop_button.ctl
TypeString: VI
VIType: Control VI
CodeSize: N/A
Callers: absolute_time_graph.lvlib:test.vi, 
Description: Typedef for time stop point enum button (custom range).